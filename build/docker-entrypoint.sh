#!/bin/bash
set -euo pipefail

# Start the automatic installation script for the latest version of Gnuboard
if [ ! -e gnuboard ]; then
  mkdir gnuboard
  cd gnuboard
  curl -o gnuboard.tar.gz -fSL https://github.com/xzneozx96/gram-study-gnuboard/tarball/main
  tar -xf gnuboard.tar.gz -C ./ --strip-components=1
  rm gnuboard.tar.gz
  mkdir data
  chown -R www-data:www-data ./
  chmod -R 777 data
  sed -i "s/'utf8'/'utf8mb4'/g" config.php
  cd ..
fi
# End of Gnuboard automatic installation script

exec "$@"
