#!/usr/bin/env bash

if [ "$1" == "start" ]; then
  docker-compose start
fi

if [ "$1" == "stop" ]; then
  docker-compose stop
fi

if [ "$1" == "update" ]; then
  echo '🔧 Updating source code...'

  cd ~/gnuboard/site
  sudo mv ./gnuboard/data ./
  sudo rm -rf gnuboard
  sudo mkdir gnuboard
  cd gnuboard
  sudo curl -o gnuboard.tar.gz -fSL https://github.com/xzneozx96/gram-study-gnuboard/tarball/main
  sudo tar -xf gnuboard.tar.gz -C ./ --strip-components=1
  sudo rm gnuboard.tar.gz
  sudo mv ~/gnuboard/site/data ./
  sudo sed -i "s/'utf8'/'utf8mb4'/g" config.php
  sudo chown -R www-data:www-data ./
  sudo chmod -R 777 data
  cd ~
fi

if [ "$1" == "docker" ]; then
  echo '🔧 Installing Docker...'

  if [ ! -x "$(command -v docker --version)" ]; then
    # Add Docker's official GPG key:
    sudo apt-get update
    sudo apt-get install -y ca-certificates curl
    sudo install -m 0755 -d /etc/apt/keyrings
    sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
    sudo chmod a+r /etc/apt/keyrings/docker.asc

    # Add the repository to Apt sources:
    echo \
      "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" |
      sudo tee /etc/apt/sources.list.d/docker.list >/dev/null
    sudo apt-get update
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    sudo usermod -aG docker $USER
    newgrp docker

    if [ ! -x "$(command -v docker-compose version)" ]; then
      echo '🔧 Installing Docker-Compose...'
      sudo apt-get update
      sudo apt-get install docker-compose-plugin
    else
      echo '⏩ Skip Installing Docker-Compose'
    fi
  else
    echo '⏩ Skip Installing Docker'
  fi
fi

if [ "$1" == "setup" ]; then
  echo '🔧 Setting system...'
  echo '🧪 Checking if docker is install'

  # Get Gnuboard deploy information
  read -p "[1/6] Enter your domain (ex:mydomain.com) : " DOMAIN
  read -p "[2/6] Enter your email address for SSL certificate (ex:myemail@email.com) : " EMAIL
  read -p "[3/6] Enter your root database password (ex:rootdbpassword) : " ROOTDBPASSWORD
  read -p "[4/6] Enter your database username for gnuboard (ex:dbuser) : " DATABASEUSER
  read -p "[5/6] Enter your database password for gnuboard (ex:dbpassword) : " DATABASEPASSWORD
  read -p "[6/6] Enter your database name for gnuboard (ex:gnuboard) : " DATABASE

  echo '📥 Cloning repo docker gnuboard'
  rm -rf gnuboard
  git clone git@gitlab.com:Jacger/gram-study-guboard.git gnuboard
  cd gnuboard

  echo '📁 Replacing deploy information to files:'

  echo '.env'
  sed -i "s/<rootdbpassword>/$ROOTDBPASSWORD/g" .env
  sed -i "s/<databaseuser>/$DATABASEUSER/g" .env
  sed -i "s/<databasepassword>/$DATABASEPASSWORD/g" .env
  sed -i "s/<database>/$DATABASE/g" .env

  echo 'docker-compose.yml'
  sed -i "s/<domain>/$DOMAIN/g" docker-compose.yml
  sed -i "s/<email>/$EMAIL/g" docker-compose.yml

  sed -i "s/<domain>/$DOMAIN/g" docker-compose.production.yml

  sed -i "s/<domain>/$DOMAIN/g" nginx/conf.d/ssl-conf

  echo 'nginx/conf.d/gnuboard.conf'
  sed -i "s/<domain>/$DOMAIN/g" nginx/conf.d/gnuboard.conf

  echo 'nginx/conf.d/phpmyadmin.conf'
  sed -i "s/<domain>/$DOMAIN/g" nginx/conf.d/phpmyadmin.conf

  echo 'Setting timezone to Asia/Ho_Chi_Minh...'
  sudo timedatectl set-timezone Asia/Ho_Chi_Minh

  echo 'Installing SSL...'
  sudo docker-compose up

  echo '📖 Preparing docker-compose up'
  sudo docker-compose down -v # Run script to store cert in `/etc/letsencrypt` folder
  sudo mkdir -p /usr/share/nginx/html

  curl -sSLo nginx/conf.d/options-ssl-nginx https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf
  sudo chmod a+x build/docker-entrypoint.sh
  sudo chmod a+x dc
  mv docker-compose.yml docker-compose.ssl.yml
  mv docker-compose.production.yml docker-compose.yml

  # Creating cron job to update certbot certificates
  echo '🔧 Configuring cron...'
  # echo "0 23 * * * docker start certbot >> /var/log/docker_cron.log 2>&1 5 23 * * * docker exec nginx nginx -s reload >> /var/log/docker_cron.log 2>&1" >>mycron
  # sudo crontab mycron
  # rm mycron

  echo '✨ Starting Docker ... ✨'
  sudo docker-compose up -d

  echo 'Done! 🎉'

  echo 'Below is a list of containers.'
  sudo docker-compose ps

  echo 'Access your gnuboard: https://'$DOMAIN
  echo 'Access your phpmyadmin: https://pma.'$DOMAIN
fi
