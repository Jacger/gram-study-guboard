# docker-gnuboard

These are some reasons why use this Docker Compose in your `Gnuboard` production environment.

☝️ 2 command to install

⚡ Performance Optimized

🔒 SSL auto-renewed

🆕 Latest docker images and Gnuboard

🚧 Phpmyadmin for database administration

🚢 Portainer for docker container management

## Stack

- Ubuntu 20.04 LTS or Centos 8
- Gnuboard latest(including rewrite configuration)
- Php7.4 latest docker image(alpine)
- MariaDB latest docker image
- Redis latest docker image(alpine)
- Nginx latest docker image(alpine)
- Certbot latest docker image
- Phpmyadmin latest docker image(fpm-alpine)
- Portainer latest docker image
- Docker
- Docker-compose

## How to use this source ?

Make sure that your `domain` and `www.domain` and `pma.domain` and `port.domain` are pointing to your server IP.

| Type | Name       | Content         |
| ---- | ---------- | --------------- |
| A    | domain.com | 123.123.123.123 |
| A    | pma        | 123.123.123.123 |
| A    | port       | 123.123.123.123 |
| A    | www        | 123.123.123.123 |

Open `80`,`443` port for connect.

Copy this command below and run it inside your new server.

### For Ubuntu 20.04 LTS users

```bash
sudo apt update -y && sudo apt upgrade -y && sudo apt install curl git cron -y && sudo apt autoremove -y
```

```bash
curl -o dc https://gitlab.com/Jacger/gram-study-guboard/-/raw/main/dc && bash dc setup && rm -f dc
```

---

## How do I use this source locally and non SSL on dev environments?

### For Ubuntu 20.04 LTS users

```bash
sudo apt update -y && sudo apt upgrade -y && sudo apt install curl git -y && sudo apt autoremove -y
```

```bash
curl -o dcl https://gitlab.com/Jacger/gram-study-guboard/-/raw/main/dcl && bash dcl setup && rm -f dcl
```

---

## Gnuboard Install Configuration

### MySQL information

Host : `db`

User : `YOUR DATABASE USERNAME`

Password : `YOUR DATABASE PASSWORD`

DB : `YOUR DATABASE NAME`

## URLs

Gnuboard : https://yourdomain.com

Phpmyadmin : https://pma.yourdomain.com

Portainer : https://port.yourdomain.com

## Commands

| Commands      | Description                       |
| ------------- | --------------------------------- |
| `./dc start`  | Start your containers             |
| `./dc stop`   | Stop all containers               |
| `./dc docker` | Install Docker and Docker Compose |

## How to import sql file to Maria Db

First, Copy sql file to server:

```shell
scp -i path/to/pem-file path/to/sql-file ubuntu@public-ip-address:~/
```

Connect to server using SSH.

```shell
ssh -i path/to/pem-file ubuntu@public-ip-address:~/
```

On server, run `docker ps -a`, find Mariadb container, copy container ID and run this command:

```shell
docker exec -it <mariadb-container-id> sh
```

In Maria DB container, check if gnuboard database is present, then delete it, and create a new one:

```shell
mariadb -u root -p

...

drop database <gnuboard-database-name>; create database <gnuboard-database-name>
```

Then exit; and run this command to import sql file to Mariadb:

```shell
mariadb -u root -p gnuboard < mysql-file.sql
```
