#!/usr/bin/env bash

if [ "$1" == "start" ]; then
    docker compose start
fi

if [ "$1" == "stop" ]; then
    docker compose stop
fi

if [ "$1" == "docker" ]; then
    echo '🔧 Installing Docker...'

    if [ ! -x "$(command -v docker --version)" ]; then
        # Add Docker's official GPG key:
        sudo apt-get update
        sudo apt-get install -y ca-certificates curl
        sudo install -m 0755 -d /etc/apt/keyrings
        sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
        sudo chmod a+r /etc/apt/keyrings/docker.asc

        # Add the repository to Apt sources:
        echo \
            "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" |
            sudo tee /etc/apt/sources.list.d/docker.list >/dev/null
        sudo apt-get update
        sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
        sudo usermod -aG docker $USER
        newgrp docker

        if [ ! -x "$(command -v docker-compose version)" ]; then
            echo '🔧 Installing Docker-Compose...'
            sudo apt-get update
            sudo apt-get install docker-compose-plugin
        else
            echo '⏩ Skip Installing Docker-Compose'
        fi
    else
        echo '⏩ Skip Installing Docker'
    fi
fi

if [ "$1" == "setup" ]; then
    echo 'Setting system...'

    # Get Gnuboard deploy information
    echo 'Getting deploy information:'
    read -p "[1/8] Enter your domain (ex : mydomain.com or localhost) : " DOMAIN
    read -p "[2/8] Enter your root database password (ex : rootdbpassword) : " ROOTDBPASSWORD
    read -p "[3/8] Enter your database username for gnuboard (ex : dbuser) : " DATABASEUSER
    read -p "[4/8] Enter your database password for gnuboard (ex : dbpassword) : " DATABASEPASSWORD
    read -p "[5/8] Enter your database name for gnuboard (ex : gnuboard) : " DATABASE
    read -p "[6/8] Which port do you want to access the web server on? (ex : 80) " WEB_PORT
    read -p "[7/8] Which port do you want to access to Portainer? (ex : 9000) " PORTAINER_PORT
    read -p "[8/8] Which port do you want to access to Phpmyadmin? (ex : 8080) " PMA_PORT

    echo 'Cloning repo docker gnuboard'
    rm -rf gnuboard
    git clone git@gitlab.com:Jacger/gnuboard5.git gnuboard
    cd gnuboard

    echo 'Renaming file and folder...'
    mv docker-compose.yml docker-compose.ssl.yml
    mv docker-compose.local.yml docker-compose.yml
    mv nginx/conf.d nginx/conf.d-ssl
    mv nginx/conf.d-local nginx/conf.d

    echo 'Replacing deploy information to files:'
    echo '.env'
    sed -i "s/<rootdbpassword>/$ROOTDBPASSWORD/g" .env
    sed -i "s/<databaseuser>/$DATABASEUSER/g" .env
    sed -i "s/<databasepassword>/$DATABASEPASSWORD/g" .env
    sed -i "s/<database>/$DATABASE/g" .env

    echo 'docker-compose.yml'
    sed -i "s/<domain>/$DOMAIN/g" docker-compose.yml
    sed -i "s/<pma_port>/$PMA_PORT/g" docker-compose.yml
    sed -i "s/<portainer_port>/$PORTAINER_PORT/g" docker-compose.yml

    echo 'nginx/conf.d/gnuboard.conf'
    sed -i "s/<domain>/$DOMAIN/g" nginx/conf.d/gnuboard.conf

    echo 'nginx/conf.d/phpmyadmin.conf'
    sed -i "s/<domain>/$DOMAIN/g" nginx/conf.d/phpmyadmin.conf
    sed -i "s/<pma_port>/$PMA_PORT/g" nginx/conf.d/phpmyadmin.conf

    echo 'nginx/conf.d/gnuboard.conf'
    sed -i "s/<web_port>/$WEB_PORT/g" nginx/conf.d/gnuboard.conf

    echo 'Setting timezone to Asia/Ho_Chi_Minh...'
    sudo timedatectl set-timezone Asia/Ho_Chi_Minh

    chmod a+x build/docker-entrypoint.sh
    chmod a+x dc

    echo 'Starting Docker ...'
    docker compose up -d

    echo 'Done! 🎉'

    echo 'Below is a list of containers.'
    docker compose ps

    echo 'Access your gnuboard: http://'$DOMAIN':'$WEB_PORT
    echo 'Access your phpmyadmin: http://'$DOMAIN':'$PMA_PORT
    echo 'Access your portainer: http://'$DOMAIN':'$PORTAINER_PORT
fi
